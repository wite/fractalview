package badwithnames.FractalView;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.Deque;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings( "serial" )
public class FractalView extends JPanel implements MouseListener, KeyListener {
  private BufferedImage     fieldBuffer;
  private int               fieldRes = 1024;
  private Model             model;
  private Deque<PointDatum> results;
  private Timer             t;
  
  public FractalView() {
    super();
    setPreferredSize( new Dimension( 800, 800 ));
    addMouseListener( this );
    fieldBuffer = new BufferedImage( fieldRes, fieldRes, BufferedImage.TYPE_INT_ARGB );
    ((Graphics2D)fieldBuffer.getGraphics()).setBackground( Color.white );
    results = new LinkedList<PointDatum>();
    model = new Model( fieldRes/Model.blockSize, results );
    
    Timer t = new Timer( 30, new ActionListener() { public void actionPerformed( ActionEvent e ){ repaint(); }});
    t.setInitialDelay( 30 );
    t.start();
    model.start();
  }
  
  @Override
  public void paintComponent( Graphics gr ){
    Graphics2D g = (Graphics2D)gr;
    Graphics2D fieldg = (Graphics2D)fieldBuffer.getGraphics();
    //g.setRenderingHint( RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR );
    
    PointDatum p;
    while( !results.isEmpty() ){
      p = results.remove();
      fieldg.setColor( getColor( p.iterations ));
      fieldg.fillRect( p.x, p.y, p.width, p.width );
    }
    
    g.drawImage( fieldBuffer, 0, 0, getWidth(), getHeight(), null );
  }

  @Override
  public void mousePressed(MouseEvent e) {
    double x = (double)e.getX() / getWidth();
    double y = (double)e.getY() / getHeight();
    if( e.getButton() == MouseEvent.BUTTON1 ){
      model.zoom( x, y, 1.0/3 );
    } else if( e.getButton() == MouseEvent.BUTTON2 ){
      model.move( x, y );
    } else if( e.getButton() == MouseEvent.BUTTON3 ){
      model.zoom( x, y, 3 );
    }
  }

  @Override
  public void keyPressed(KeyEvent e) {
    if( e.getKeyCode() == KeyEvent.VK_R ){
      model.resetField();
    }
  }
  
  private Color getColor( int i ){
    if( i == 0 )
      return Color.black;
    
    final int[][] colors = {{ 0,0,0 },{ 44,50,69 },{ 255,255,255 },{ 189,181,34 },{ 122,54,23 }};
    int res = 4;
    int iters = i % ( res*colors.length );
    int first = iters / res;
    int second = ( first + 1 ) % colors.length;
    iters -= first*res;
    int[] rgb = new int[3];
    for( i = 0; i < 3; ++i ){
      rgb[i] = colors[first][i] + iters*( colors[second][i] - colors[first][i] )/res;
    }
    return new Color( rgb[0], rgb[1], rgb[2] );
  }
  
	public static void main( String[] args ){
		FractalView fv = new FractalView();
	  JFrame frame = new JFrame( "FractalView" );
	  frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
	  frame.addKeyListener( fv );
		frame.add( fv );
		frame.pack();
		frame.setVisible( true );
	}

  @Override
  public void mouseClicked(MouseEvent e) {}

  @Override
  public void mouseReleased(MouseEvent e) {}

  @Override
  public void mouseEntered(MouseEvent e) {}

  @Override
  public void mouseExited(MouseEvent e) {}

  @Override
  public void keyTyped(KeyEvent e) {}

  @Override
  public void keyReleased(KeyEvent e) {}
}
/*
  @Override
  public void componentResized( ComponentEvent e ) {
    fieldBuffer = new BufferedImage( getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB );
    calc();
  }

  @Override
  public void componentMoved( ComponentEvent e ) {}

  @Override
  public void componentShown( ComponentEvent e ) {}

  @Override
  public void componentHidden( ComponentEvent e ) {}
*/

