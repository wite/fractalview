package badwithnames.FractalView;

public class PointDatum {
  int iterations;
  int width;
  int x;
  int y;
  
  public PointDatum( int x, int y, int width ){
    this.x = x;
    this.y = y;
    iterations = 0;
    this.width = width;
  }
}
