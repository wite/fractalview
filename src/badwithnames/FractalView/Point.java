package badwithnames.FractalView;

public class Point {
  int i;
  double cRe;
  double cIm;
  double zRe;
  double zIm;
  
  public Point( double real, double imaginary ) {
    i = 1;
    cRe = real;
    cIm = imaginary;
    zRe = cRe;
    zIm = cIm;
  }
  
  public void iterate() {
    double r = zRe;
    zRe = r*r - zIm*zIm + cRe;
    zIm = 2*r*zIm + cIm;
    ++i;
  }
  
  public double magSq() {
    return zRe*zRe + zIm*zIm;
  }
  
  public double magnitude() {
    return Math.sqrt( magSq() );
  }
}
