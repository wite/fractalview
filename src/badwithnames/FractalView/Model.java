package badwithnames.FractalView;

import java.util.Deque;


public class Model {
  public static final int   blockSize = 32;
  private double            cR;            // top left corner real coord
  private double            cI;            // top left corner imaginary coord
  private int               resolution;
  private Deque<PointDatum> results;
  private double            scale;
  private ModelWorker[][]   workers;
  
  public Model( int resolution, Deque<PointDatum> results ){
    cR = -2.0;
    cI = 2.0;
    scale = 4.0;    //starting width of the field for an escape radius of 2
    this.resolution = resolution;
    this.results = results;
    workers = new ModelWorker[resolution][resolution];
    resetPoints();
  }
  
  public void zoom( double x, double y, double scale ){
    stop();
    results.clear();
    cR += ( x - 0.5*scale )*this.scale;
    cI -= ( y - 0.5*scale )*this.scale;
    this.scale *= scale;
    resetPoints();
    start();
  }
  
  public void move( double x, double y ){ zoom( x, y, 1 ); }
  
  public void start() {
    for( int i = 0; i < workers.length; ++i ){
      for( int j = 0; j < workers.length; ++j ){
        workers[i][j].execute();
      }
    }
  }
  
  public void stop() {
    for( int i = 0; i < workers.length; ++i ){
      for( int j = 0; j < workers.length; ++j ){
        workers[i][j].cancel( false );
      }
    }
  }
  
  public void resetField() {
    stop();
    results.clear();
    cR = -2.0;
    cI = 2.0;
    scale = 4.0;
    resetPoints();
    start();
  }
  
  private void resetPoints() {
    Point[][] point;
    double px;
    double py;
    for( int i = 0; i < resolution; ++i ){
      for( int j = 0; j < resolution; ++j ){
        point = new Point[blockSize][blockSize];
        for( int x = 0; x < blockSize; ++x ){
          for( int y = 0; y < blockSize; ++y ){
            px = cR + ( i*blockSize + x )*scale/resolution/blockSize;
            py = cI - ( j*blockSize + y )*scale/resolution/blockSize;
            point[x][y] = new Point( px, py );
          }
          workers[i][j] = new ModelWorker( i*blockSize, j*blockSize, point, 10000, blockSize, results );
        }
      }
    }
  }
}
